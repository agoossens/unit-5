import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 *Algorithm to solve the problem
 *the same algorithm as the flow layout, except this time use border layout 
 *then when adding the panels chose the correct locations (NORTH,SOUTH etc.)
 *create a demo class to run the project 
 * @author alexgoossens
 *
 */
public class borderLayout extends JFrame {

	JButton buttonOne;
	JButton buttonTwo;
	JButton buttonThree;
	JButton buttonFour;
	JButton buttonFive;
	JButton buttonSix;
	JPanel panel1;
	JPanel panel2;
	/**
	 * creates the JButtons and JPanels that are required for this project
	 */

	public borderLayout(){
		super("BorderLayout Demo");
		/**
		 * sets the title to "BorderLayout Demo"
		 */
		setLayout(new BorderLayout());
		/**
		 * sets the layout to border layout
		 */
		buttonOne = new JButton("Button 1");

		buttonTwo = new JButton("Button 2");

		buttonThree = new JButton("Button 3");

		buttonFour = new JButton("Button 4");

		buttonFive = new JButton("Button 5");

		buttonSix = new JButton("Button 6");
		/**
		 * gives values to the different JButtons
		 */

		panel1 = new JPanel();

		panel1.add(buttonOne);
		panel1.add(buttonTwo);
		panel1.add(buttonThree);
		this.add(panel1,BorderLayout.NORTH);

		panel2 = new JPanel();

		panel2.add(buttonFour);
		panel2.add(buttonFive);
		panel2.add(buttonSix);
		this.add(panel2,BorderLayout.SOUTH);

		/**
		 * sticks the JButtons in the panels and places the panels on the correct locations
		 */

	}
}
