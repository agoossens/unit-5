import javax.swing.JFrame;

public class gridLayoutDemo {

	public static void main(String[] args)
	{
		gridLayout example = new gridLayout();
		example.pack();
		example.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		example.setSize(300,130);
		example.setVisible(true);
		/**
		 * demo class to run the gridLayout class
		 * makes sure that it close, has the correct size and is visible
		 */
	}
}
