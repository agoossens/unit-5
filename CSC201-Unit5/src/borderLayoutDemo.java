import javax.swing.JFrame;

public class borderLayoutDemo {

	public static void main(String[] args)
	{
		borderLayout example = new borderLayout();
		example.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		example.setSize(275,180);
		example.setVisible(true);
		/**
		 * runs the borderLayout class
		 * allows it to close, sets the size and makes it visible
		 */
	}
}
