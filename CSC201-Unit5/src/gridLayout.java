import java.awt.BorderLayout;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * Algorithm to solve the problem
 * use border and gridlayout in order to do this project
 * set the panels on the correct grid and make sure they fill up
 * create a demo class to run the project
 * 
 * @author alexgoossens
 *
 */


public class gridLayout extends JFrame {

	JButton buttonOne;
	JButton buttonTwo;
	JButton buttonThree;
	JButton buttonFour;
	JButton buttonFive;
	JButton buttonSix;
	JPanel panel1;
	JPanel panel2;
/**
 * creates the individual buttons and panels
 */

	public gridLayout(){
		super("GridLayout Demo");
		/**
		 * sets the title to "GridLayout Demo"
		 */
		setLayout(new BorderLayout()) ;
		/**
		 * sets the layout to border layout
		 */
		
		buttonOne = new JButton("Button 1");

		buttonTwo = new JButton("Button 2");

		buttonThree = new JButton("Button 3");

		buttonFour = new JButton("Button 4");

		buttonFive = new JButton("Button 5");
		
		buttonSix = new JButton("Button 6");
		
		/**
		 * sets the buttons to a value 
		 */
		
		panel1 = new JPanel( new GridLayout(2,2));
	
		panel1.add(buttonOne);
		panel1.add(buttonTwo);
		panel1.add(buttonThree);
		this.add(panel1,BorderLayout.CENTER);

		
		panel2 = new JPanel( new GridLayout(2,2));
		
		panel2.add(buttonFour);
		panel2.add(buttonFive);
		panel2.add(buttonSix);
		this.add(panel2,BorderLayout.NORTH);

		/**
		 * adds the buttons to the panels which are placed in the correct grid and location
		 */
		
	}
}
