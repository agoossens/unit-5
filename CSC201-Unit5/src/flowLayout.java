/**
 * Algorithm to solve the problem
 * create jbuttons and jpanels, define them and place them in the correct spot
 * create a demo class in order to run this project
 */
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

import javax.swing.JPanel;


/**
 * 
 * @author alexgoossens
 *5 points
 */
public class flowLayout extends JFrame {

	JButton buttonOne;
	JButton buttonTwo;
	JButton buttonThree;
	JButton buttonFour;
	JButton buttonFive;
	JButton buttonSix;
	JPanel panel1;
	JPanel panel2;
/**
 * JButton - creates the button boxes
 * JPanel - where you store the buttons
 */

	public flowLayout(){
		super("FlowLayout Demo");
		/**
		 * sets the title to "FlowLayout Demo"
		 */
		setLayout(new FlowLayout());
		/**
		 * sets the layout type
		 */

		buttonOne = new JButton("Button 1");

		buttonTwo = new JButton("Button 2");

		buttonThree = new JButton("Button 3");

		buttonFour = new JButton("Button 4");

		buttonFive = new JButton("Button 5");
		
		buttonSix = new JButton("Button 6");
		/**
		 * creates the buttons and sets their individual titles
		 */
		
		panel1 = new JPanel();
		this.add(panel1);
		panel1.add(buttonOne);
		panel1.add(buttonTwo);
		panel1.add(buttonThree);
		
		panel2 = new JPanel();
		this.add(panel2);
		panel2.add(buttonFour);
		panel2.add(buttonFive);
		panel2.add(buttonSix);
		
		/**
		 * places the buttons in the correct panel
		 */
		
		
		
	}
}
