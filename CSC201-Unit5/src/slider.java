import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.BorderLayout;

/**
 * Algorithm to solve the problem
 * create sliders, buttons and panels
 * set each of the above to the correct values
 * stick them in the correct panels and place the panels in the correct locations
 * @author alexgoossens
 *
 */

public class slider extends JFrame {

	JSlider redSlide;
	JSlider greenSlide;
	JSlider blueSlide;
	
	JLabel redLabel;
	JLabel greenLabel;
	JLabel blueLabel;
	JLabel chooseColor; 
	JLabel showColors;
	
	JPanel colorScheme, slider, labels;
	/**
	 * initiates the sliders, labels and panels
	 */
	
	public slider()
	{

		super("SliderColor Demo");
		redSlide = new JSlider(JSlider.HORIZONTAL,0,200,0);
		greenSlide = new JSlider(JSlider.HORIZONTAL,0,200,0);
		blueSlide = new JSlider(JSlider.HORIZONTAL,0,200,0);
		/**
		 * creates the sliders, sets them to the correct size and location
		 */
		
		redLabel = new JLabel("Red");
		greenLabel = new JLabel("Green");
		blueLabel = new JLabel("Blue");
		chooseColor = new JLabel("Choose Colors");
		showColors = new JLabel("Show Color");
		/**
		 * creates the buttons and gives them the correct names
		 */
		
		event colorChange = new event();
		redSlide.addChangeListener(colorChange);
		greenSlide.addChangeListener(colorChange);
		blueSlide.addChangeListener(colorChange);
		/**
		 * makes the sliders respond in order to change the colors
		 */
		
		colorScheme = new JPanel();
		colorScheme.add(showColors);
		/**
		 * creates a new panel and adds a JButton in it
		 */
		
		Container pane = this.getContentPane();
		pane.setLayout(new GridLayout(1,3,3,3));
		/**
		 * creates a container in which the panels are placed
		 */
		
		
		slider = new JPanel();
		labels = new JPanel();
		
		
		pane.add(slider);
		pane.add(labels);
		pane.add(colorScheme);
		/**
		 * stores the panels into the container pane
		 */
		
		labels.setLayout(new GridLayout(3,1,2,2));
		labels.add(redLabel);
		labels.add(greenLabel);
		labels.add(blueLabel);	
		
		slider.setLayout(new GridLayout(3,1,3,3));
		slider.add(redSlide);
		slider.add(greenSlide);
		slider.add(blueSlide);
		
	}
	public class event implements ChangeListener 
	{
		public void stateChanged(ChangeEvent e)
		{
			int r = redSlide.getValue();
			int g = greenSlide.getValue();
			int b = blueSlide.getValue();
			
			/**
			 * gets the value of the color slider
			 */
			redLabel.setText("red" );
			greenLabel.setText("green" );
			blueLabel.setText("blue" );
			
			
			colorScheme.setBackground(new Color(r,g,b));
			/**
			 * sets the color square to the adjusted colors
			 */
		}
	}
	
}
